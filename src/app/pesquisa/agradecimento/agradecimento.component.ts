import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agradecimento',
  templateUrl: './agradecimento.component.html',
  styleUrls: ['./agradecimento.component.css']
})
export class AgradecimentoComponent implements OnInit {

  constructor( public router: Router) { }

  ngOnInit() {
    setTimeout(()=>{
      this.router.navigate([''])
    },5000)
  }

}
