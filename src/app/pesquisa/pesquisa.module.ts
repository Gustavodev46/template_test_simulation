import { PesquisaRoutingModule } from './pesquisa-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvaliacaoComponent } from './avaliacao/avaliacao.component';
import { AgradecimentoComponent } from './agradecimento/agradecimento.component';

@NgModule({
  declarations: [
    AvaliacaoComponent,
    AgradecimentoComponent
  ],
  imports: [
    CommonModule,
    PesquisaRoutingModule
  ],
  exports: [
    AvaliacaoComponent
  ]
})
export class PesquisaModule { }
