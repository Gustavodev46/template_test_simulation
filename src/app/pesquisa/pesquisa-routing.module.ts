import { AgradecimentoComponent } from './agradecimento/agradecimento.component';
import { AvaliacaoComponent } from './avaliacao/avaliacao.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {  path: '',  component: AvaliacaoComponent },
  {  path: 'avaliacao',  component: AvaliacaoComponent },
  {  path: 'agradecimento',  component: AgradecimentoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PesquisaRoutingModule { }
